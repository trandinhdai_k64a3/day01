USE QLSV;

INSERT INTO DMKHOA (MaKH, TenKhoa)
VALUES ('CNTT', 'Công nghệ thông tin');

INSERT INTO DMKHOA (MaKH, TenKhoa)
VALUES ('KTPM', 'Kỹ thuật phần mềm');

INSERT INTO DMKHOA (MaKH, TenKhoa)
VALUES ('KTTT', 'Kỹ thuật thông tin');

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV001', 'Nguyễn', 'Văn A', 'Nam', '1990-01-01', 'Hà Nội', 'Hà Nội', 'CNTT', 1000000);

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV002', 'Nguyễn', 'Văn B', 'Nam', '1990-01-01', 'Nam Định', 'Hà Nội', 'CNTT', 1000000);

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV003', 'Nguyễn', 'Văn C', 'Nam', '1990-01-01', 'Hải Phòng', 'Hải Phòng', 'KTPM', 1000000);

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV004', 'Nguyễn', 'Văn D', 'Nam', '1990-01-01', 'Hà Nội', 'Hà Nội', 'KTTT', 1000000);

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV005', 'Nguyễn', 'Văn E', 'Nam', '1990-01-01', 'TP HCM', 'TP HCM', 'KTTT', 1000000);

INSERT INTO SINHVIEN (MaSV, HoSV, TenSV, GioiTinh, NgaySinh, NoiSinh, DiaChi, MaKH, HocBong)
VALUES ('SV006', 'Nguyễn', 'Văn F', 'Nam', '1990-01-01', 'Hà Nội', 'Hà Nội', 'KTTT', 1000000);